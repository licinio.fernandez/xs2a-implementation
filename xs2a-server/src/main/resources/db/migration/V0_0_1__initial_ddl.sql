-- Example database migration

create table examples ( 
  id bigint not null,
  value  varchar(100)
);

comment on column examples.value is 'Example value column';

alter table examples add constraint examples_pk primary key (id);

insert into examples values (1, 'example');