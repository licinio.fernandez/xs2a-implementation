package es.ubu.ii.xs2a.server.persistence.repositories;

import org.springframework.data.repository.CrudRepository;

import es.ubu.ii.xs2a.server.persistence.entities.Example;

public interface ExampleRepository extends CrudRepository<Example, Long>{

}
