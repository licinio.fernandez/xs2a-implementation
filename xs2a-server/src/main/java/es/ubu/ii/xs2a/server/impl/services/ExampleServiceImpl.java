package es.ubu.ii.xs2a.server.impl.services;

import org.springframework.stereotype.Service;

import es.ubu.ii.xs2a.server.api.dtos.ExampleDto;
import es.ubu.ii.xs2a.server.api.exceptions.NotFoundException;
import es.ubu.ii.xs2a.server.api.services.ExampleService;
import es.ubu.ii.xs2a.server.impl.mappers.ExampleMapper;
import es.ubu.ii.xs2a.server.persistence.repositories.ExampleRepository;
import lombok.RequiredArgsConstructor;

/**
 * @author Licinio Fernández
 *
 */
@Service
@RequiredArgsConstructor
public class ExampleServiceImpl implements ExampleService {

	private final ExampleMapper exampleMapper;
	
	private final ExampleRepository exampleRepository;
	
	@Override
	public ExampleDto getExampleById(Long id) {		
		return exampleMapper.toDto(exampleRepository.findById(id).orElseThrow(() -> new NotFoundException()));
	}

}
