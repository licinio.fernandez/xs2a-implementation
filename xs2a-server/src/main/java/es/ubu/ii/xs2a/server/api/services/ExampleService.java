package es.ubu.ii.xs2a.server.api.services;

import es.ubu.ii.xs2a.server.api.dtos.ExampleDto;

/**
 * Example service definition.
 * 
 * @author Licinio Fernández
 *
 */
public interface ExampleService {

	/**
	 * Example service method.
	 * 
	 * @param id 
	 * 
	 * @return an example
	 */
	ExampleDto getExampleById(Long id);

}
