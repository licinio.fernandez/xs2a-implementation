package es.ubu.ii.xs2a.server.resources;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.ubu.ii.xs2a.server.api.dtos.ExampleDto;
import es.ubu.ii.xs2a.server.api.services.ExampleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Example resource.
 */
@RestController
@RequestMapping(path = "/examples")
@Slf4j
@RequiredArgsConstructor
public class ExampleResource {

	private final ExampleService exampleService;

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ExampleDto> getExampleById(@PathVariable("id") Long id) {
		log.debug("Requesting resource {}", id);
		return ResponseEntity.ok(exampleService.getExampleById(id));
	}

}
