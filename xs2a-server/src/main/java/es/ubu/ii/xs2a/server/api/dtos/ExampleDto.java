package es.ubu.ii.xs2a.server.api.dtos;

import lombok.Data;

/**
 * Example DTO.
 * 
 * @author Licinio Fernández
 *
 */
@Data
public class ExampleDto {

	private Long id;
	
	private String value;
		
}
