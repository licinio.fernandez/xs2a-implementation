package es.ubu.ii.xs2a.server.persistence.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Licinio Fernández
 *
 */
@Getter
@Setter
@Entity
@Table(name="examples")
public class Example {

	@Id
	private Long id;
	
	private String value;
}
