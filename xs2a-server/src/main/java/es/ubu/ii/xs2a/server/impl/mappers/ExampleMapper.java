package es.ubu.ii.xs2a.server.impl.mappers;

import org.springframework.stereotype.Component;

import es.ubu.ii.xs2a.server.api.dtos.ExampleDto;
import es.ubu.ii.xs2a.server.persistence.entities.Example;

/**
 * Example mapper.
 * 
 * @author Licinio Fernández
 *
 */
@Component
public class ExampleMapper {

	public ExampleDto toDto(Example entity) {
		ExampleDto result = new ExampleDto();
		result.setId(entity.getId());
		result.setValue(entity.getValue());
		return result;
	}
	
}
