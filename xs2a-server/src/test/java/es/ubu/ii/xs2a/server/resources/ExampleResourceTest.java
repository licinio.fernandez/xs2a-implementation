package es.ubu.ii.xs2a.server.resources;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import es.ubu.ii.xs2a.server.api.dtos.ExampleDto;
import es.ubu.ii.xs2a.server.api.services.ExampleService;

@ExtendWith(MockitoExtension.class)
public class ExampleResourceTest {

	@Mock
	ExampleService exampleService;
	
	@InjectMocks
	ExampleResource exampleResource;

	@Test
	public void shouldGetExampleById() {
		// given
		Mockito.when(exampleService.getExampleById(Mockito.anyLong())).thenReturn(new ExampleDto());
		// when
		ResponseEntity<ExampleDto> result = exampleResource.getExampleById(1l);
		// then
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}

}
